tp2-ghostbusters
================
Nombres de los integrantes del Grupo:
-------------------------------------
Abinet German, Fernandez Lucas, Gamboa Marcelo y Ledesma Javier.

Desiciones de diseño tomadas:
-----------------------------
Para resolver el problema de las Legiones que compart�an mucha funcionalidad con las unidades, se opt� por aplicar el patr�n Composite, de modo tal que la clase Legion pueda adoptar la funcionalidad de Unidad y sobreescribirla de acuerdo a su necesidad.
En cuanto al dado, aplicamos el patr�n Singleton para que la instancia sea �nica, es decir, que los 2 jugadores esten tirando el mismo dado.

Descripcion de los archivos *.java:
-----------------------------------

IMenu.java: es utilizada como interface de los men�es, ya que todos tienen el mismo comportamiento. 

MenuInicial.java: Da la bienvenida al juego, proporciona la interfaz de usuario para crear a los personajes y se tiran los dados.
                 

MenuCrearLegion.java: Permite a los jugadores armar la Legion, ya sea comprando un ejercito prearmado de los archivos o creandolo mediante la compra de unidades individuales.
                    
MenuAtaque.java: Permite a un jugador atacar a su oponente o reabastecer su ejercito antes de atacar (Siempre cuando el jugador tenga creditos disponibles). Una vez que un jugador gana, muestra el mensaje correspondiente y termina el juego.

CustomFileReader: Clase abstracta que permite leer un archivo seg�n el path parametrizado en el constructor y un delimitador.

FCReader: Clase que hereda de CustomFileReader. Se encarga de leer archivos en formato CSV, campos separados por ','.

FPCReader: Clase que hereda de CustomFileReader. Se encarga de leer archivos en formato CSV, campos separados por ';'.
                  
Juego.java: Posee de atributos a los jugadores y permite el ataque de un jugador a otro. Es el encargado de mostrar el men� inicial al iniciar el juego, asignar los jugadores, evaluar el turno del jugador, determinar si hay o no un ganador y devolver el nombre del ganador.

Dado.java: Es una �nica instancia (patr�n Singleton) para que los jugadores siempre lancen el mismo dado.

Jugador.java: Posee su legion, un nombre y la cantidad de creditos. Es capaz de consumir sus creditos, evaluar, si tiene puntos de vida, devolver su nombre, devolver y obtener su legion y atacar a otro jugador.

Unidad.java: Es una clase abstracta y base para las unidades. De esta heredan las clases Auxiliar, Legionario, Centurion y Legion. Posee atributos como los puntos de ataque, la vida, el costo y la cantidad de soldados para cada unidad. Es capaz de recibir un ataque, disminuyendo sus putnos de vida en base a los puntos de ataque recibidos.

Auxiliar.java: Es un tipo de Unidad con una jabalina como arma. Tiene una chance de dos de acertar un ataque.

Legionario.java: Es un tipo de Unidad con una espada corta y escudo como armas.

Centurion.java: Es un tipo de Unidad con una espada corta y escudo como armas. Tiene una chance de dos de esquivar un ataque. Ademas, aumenta un 10% el total de da�o del ejercito por cada centuri�n presente.

Arma.java: Enum con todas las armas que utilizan las unidades.

Legion.java: Es un conjunto de Unidades, pero hereda de Unidad ya que tienen comportamientos similares. Se puede calcular su costo, su da�o, sus puntos de vida y su cantidad de unidades. Tambien permite atacar a otra legion y recibir da�o de ellas.

Conclusiones:
-------------
El uso de git nos facilit� el manejo de versiones, ya que todos necesitabamos colaborar desde nuestras casas y no teniamos oportunidades de juntarnos. Cada uno sub�a sus cambios y todos empezabamos a trabajar sobre la ultima version disponible.
Los patrones de dise�o fueron claves para poder llevar a cabo el proyecto. El patr�n Composite nos facilit� la resoluci�n de un problema muy com�n como lo es el de las Legiones y Unidades anteriormente descripto. 
Definitivamente este proyecto no sirvi� para solidificar los conocimientos vistos en clase sobre estructuras y patrones de dise�o.
            
Instalación
-----------
Clonar este repo:

    $ git clone https://gitlab.com/untref-ayp2-ghostbusters/tp2-ghostbusters.git

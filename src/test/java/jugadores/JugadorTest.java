package jugadores;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jugadores.Jugador;
import unidades.Legion;

public class JugadorTest {
	
	private Jugador jugador;
	
	@Before
	public void setup() {
		this.jugador = new Jugador("Marcelo");
	}
	
	@Test
	public void jugadorSinLegionNoTienePuntosDeVida() {
		Assert.assertFalse(jugador.tienePuntosDeVida());
	}
	
	@Test
	public void elNombreAsignadoALaLegionEsElMismoQueDevuelve() {
		this.jugador.asignarNombreLegion("Legion 1");
		Assert.assertEquals("Legion 1", this.jugador.obtenerLegion().getNombre());
	}
	
	@Test
	public void jugadorConLegionTienePuntosDeVida() {
		Legion legion = new Legion("Legion 1", 1, 1, 1);
		this.jugador.asignarLegion(legion);
		Assert.assertTrue(jugador.tienePuntosDeVida());
	}
	
	@Test
	public void legionAsignadaAlJugadorEsLaMismaQueDevuelveObtenerLegion() {
		Legion legion = new Legion("Legion 1", 1, 1, 1);
		this.jugador.asignarLegion(legion);
		Assert.assertEquals(legion.obtenerCantidadUnidades(), this.jugador.obtenerLegion().obtenerCantidadUnidades());
	}
	
	@Test
	public void legionAsignadaAlJugadorConUnaLegionExistenteSeSumanLasCantidadesDeUnidades() {
		Legion legion = new Legion("Legion 1", 1, 1, 1);
		Legion legion2 = new Legion("Legion 2", 1, 1, 1);
		this.jugador.asignarLegion(legion);
		this.jugador.asignarLegion(legion2);
		int cantidadUnidadesTotal = legion.obtenerCantidadUnidades() + legion2.obtenerCantidadUnidades();
		Assert.assertEquals(cantidadUnidadesTotal, this.jugador.obtenerLegion().obtenerCantidadUnidades());
	}
	
	@Test
	public void getNombreDevuelveMarcelo() {
		Assert.assertEquals("Marcelo", this.jugador.getNombre());
	}
	
	@Test
	public void getCreditosSinGastarDevuelve500000() {
		Assert.assertEquals(500000, this.jugador.getCreditos());
	}
	
	@Test
	public void gasta100000CreditosYDevuelve400000() {
		Assert.assertTrue(this.jugador.consumirCreditos(100000));
		Assert.assertEquals(400000, this.jugador.getCreditos());
	}
	
	@Test
	public void noPuedeGastarMasCreditosDeLosQueTiene() {
		Assert.assertFalse(this.jugador.consumirCreditos(600000));
		Assert.assertEquals(500000, this.jugador.getCreditos());
	}
	
	@Test
	public void jugadorAtacaAOtroJugador() {
		Jugador jugador2 = new Jugador("German");
		jugador2.asignarLegion(new Legion("Legion Nazi", 1, 1, 1));
		this.jugador.atacar(jugador2);
	}
	
}

package unidades;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UnidadesTest {

    private Legion legionJugador1;
    private Legion legionJugador2;
    private final double PUNTOS_ATAQUE_AUXILIAR = 0.7;
    private final double PUNTOS_ATAQUE_LEGIONARIO = 1.4;
    private final double PUNTOS_ATAQUE_CENTURION = 1;
    private final int COSTO_AUXILIAR = 50;
    private final int COSTO_LEGIONARIO = 100;
    private final int COSTO_CENTURION = 200;
    private final int PUNTOS_VIDA_UNIDAD = 100;
    private final double PORCENTAJE_AUMENTO_ATAQUE_CENTURION = 0.1;

    @Before
    public void setup() {
        legionJugador1 = new Legion("Legion 1", 1, 1, 1);
        legionJugador2 = new Legion("Legion 2", 1, 1, 1);
    }

    @Test
    public void CreoLegionConUnaUnidadDeCadaUnaCorrectamente() {
        double puntosAtaqueTotal = (PUNTOS_ATAQUE_AUXILIAR / 2) + PUNTOS_ATAQUE_LEGIONARIO + PUNTOS_ATAQUE_CENTURION;
        puntosAtaqueTotal += puntosAtaqueTotal * PORCENTAJE_AUMENTO_ATAQUE_CENTURION;

        int costoTotal = COSTO_AUXILIAR + COSTO_LEGIONARIO + COSTO_CENTURION;

        Assert.assertEquals(puntosAtaqueTotal, legionJugador1.obtenerPuntosDeAtaqueTotal(), 0.01);
        Assert.assertEquals(3 * PUNTOS_VIDA_UNIDAD, legionJugador1.obtenerPuntosDeVidaTotal(), 0);
        Assert.assertEquals(costoTotal, legionJugador1.obtenerCostoTotal());
    }

    @Test
    public void AgregoUnaUnidadDeCadaUnaCorrectamente() {

        double puntosAtaqueTotal = (PUNTOS_ATAQUE_AUXILIAR / 2) + PUNTOS_ATAQUE_LEGIONARIO + PUNTOS_ATAQUE_CENTURION;
        puntosAtaqueTotal *= 2;
        puntosAtaqueTotal += puntosAtaqueTotal * (2 * PORCENTAJE_AUMENTO_ATAQUE_CENTURION);
        int costoTotal = 2 * (COSTO_AUXILIAR + COSTO_LEGIONARIO + COSTO_CENTURION);

        legionJugador1.agregarAuxiliares(1);
        legionJugador1.agregarLegionarios(1);
        legionJugador1.agregarCenturiones(1);

        Assert.assertEquals(puntosAtaqueTotal, legionJugador1.obtenerPuntosDeAtaqueTotal(), 0.01);
        Assert.assertEquals(6 * PUNTOS_VIDA_UNIDAD, legionJugador1.obtenerPuntosDeVidaTotal(), 0);
        Assert.assertEquals(costoTotal, legionJugador1.obtenerCostoTotal());
    }

    @Test
    public void VidaDeUnidadesAgregadasSonLosCorrectos() {
        Assert.assertEquals(PUNTOS_VIDA_UNIDAD, legionJugador1.obtenerAuxiliares().obtenerPuntosDeVidaUnitario(), 0);
        Assert.assertEquals(PUNTOS_VIDA_UNIDAD, legionJugador1.obtenerLegionarios().obtenerPuntosDeVidaUnitario(), 0);
        Assert.assertEquals(PUNTOS_VIDA_UNIDAD, legionJugador1.obtenerCenturiones().obtenerPuntosDeVidaUnitario(), 0);
    }

    @Test
    public void PuntosDeAtaqueDeUnidadesAgregadasSonLosCorrectos() {
        Assert.assertEquals(PUNTOS_ATAQUE_AUXILIAR, legionJugador1.obtenerAuxiliares().obtenerPuntosDeAtaqueUnitario(), 0);
        Assert.assertEquals(PUNTOS_ATAQUE_LEGIONARIO, legionJugador1.obtenerLegionarios().obtenerPuntosDeAtaqueUnitario(), 0);
        Assert.assertEquals(PUNTOS_ATAQUE_CENTURION, legionJugador1.obtenerCenturiones().obtenerPuntosDeAtaqueUnitario(), 0);
    }

    @Test
    public void CostosDeUnidadesAgregadasSonLosCorrectos() {
        Assert.assertEquals(COSTO_AUXILIAR, legionJugador1.obtenerAuxiliares().obtenerCostoUnitario());
        Assert.assertEquals(COSTO_LEGIONARIO, legionJugador1.obtenerLegionarios().obtenerCostoUnitario());
        Assert.assertEquals(COSTO_CENTURION, legionJugador1.obtenerCenturiones().obtenerCostoUnitario());
    }

    @Test
    public void ElNombreDeLALegionSeDevuelveCorrectamente() {
        Assert.assertEquals("Legion 1", legionJugador1.getNombre());
    }

    @Test
    public void MetodoToStringFuncionaCorrectamente() {
        double puntosAtaqueTotal = (PUNTOS_ATAQUE_AUXILIAR / 2) + PUNTOS_ATAQUE_LEGIONARIO + PUNTOS_ATAQUE_CENTURION;
        puntosAtaqueTotal += puntosAtaqueTotal * PORCENTAJE_AUMENTO_ATAQUE_CENTURION;

        Assert.assertEquals("Legion 1: 1 Auxiliares, 1 Legionarios, 1 Centuriones, " + puntosAtaqueTotal + " puntos de Ataque. Costo total: 350 creditos.", legionJugador1.toString());
    }

    @Test
    public void LaCantidadDeUnidadesAgregadasEsLaCorrecta() {
        Assert.assertEquals(3, legionJugador1.obtenerUnidades().size());
        Assert.assertEquals(3, legionJugador1.obtenerCantidadUnidades());
    }

    @Test
    public void LegionJugadorUnoGanaALegionJugadorDos() {
        double puntosAtaqueTotal = (PUNTOS_ATAQUE_AUXILIAR / 2) + PUNTOS_ATAQUE_LEGIONARIO + PUNTOS_ATAQUE_CENTURION;
        puntosAtaqueTotal += puntosAtaqueTotal * PORCENTAJE_AUMENTO_ATAQUE_CENTURION;

        legionJugador1.atacar(legionJugador2);

        Assert.assertEquals(300 - puntosAtaqueTotal, legionJugador2.obtenerPuntosDeVidaTotal(), 0);
    }

    @Test
    public void LegionGrosaLeGanaALegionJugadorDosYSuVidaQuedaEnCero() {
        Legion legionGrosa = new Legion("Legion Grosa", 300, 300, 300);

        legionGrosa.atacar(legionJugador2);

        Assert.assertEquals(0, legionJugador2.obtenerPuntosDeVidaTotal(), 0);
    }

}

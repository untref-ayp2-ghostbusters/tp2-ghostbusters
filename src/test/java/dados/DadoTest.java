package dados;

import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Test;

import dados.Dado;

public class DadoTest {
	
	@Test
	public void resultadoDadoEstaSiempreEntre1Y6() {
		LinkedList<Integer> posiblesResultados = new LinkedList<>();
		posiblesResultados.add(1);
		posiblesResultados.add(2);
		posiblesResultados.add(3);
		posiblesResultados.add(4);
		posiblesResultados.add(5);
		posiblesResultados.add(6);
		
		for (int i=0; i<10; i++)
			Assert.assertTrue(posiblesResultados.contains(Dado.obtenerInstancia().tirarDado()));
	}

}

package juegos;

import juegos.Juego;
import unidades.Legion;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class JuegoTest {
    private Juego juegoPrueba = new Juego();
    private Legion legionJugador1;
    private Legion legionJugador2;

    @Before
    public void setup() {
        legionJugador1 = new Legion("Legion 1", 2, 2, 2);
        legionJugador2 = new Legion("Legion 2", 1, 1, 1);
        juegoPrueba.setJugador1("Espartacus");
        juegoPrueba.setJugador2("Assasins");
    }

    @Test
    public void hayUnGanador() {
        Legion legionGrosa = new Legion("Legion Grosa",300,300,300);

        juegoPrueba.getJugador1().asignarLegion(legionGrosa);
        juegoPrueba.getJugador2().asignarLegion(legionJugador2);

        juegoPrueba.atacar(juegoPrueba.getJugador1() ,juegoPrueba.getJugador2());

        Assert.assertTrue(juegoPrueba.hayGanador());
    }

    @Test
    public void nombreDelGanador() {
        Legion legionGrosa = new Legion("Legion Grosa",300,300,300);

        juegoPrueba.getJugador1().asignarLegion(legionGrosa);
        juegoPrueba.getJugador2().asignarLegion(legionJugador2);

        juegoPrueba.atacar(juegoPrueba.getJugador1() ,juegoPrueba.getJugador2());

        Assert.assertEquals(juegoPrueba.getJugador1().getNombre(), juegoPrueba.obtenerNombreJugadorGanador());
    }

    @Test
    public void verificarQueSeAtaco() {

        juegoPrueba.getJugador1().asignarLegion(legionJugador1);
        juegoPrueba.getJugador2().asignarLegion(legionJugador2);

        juegoPrueba.atacar(juegoPrueba.getJugador2() ,juegoPrueba.getJugador1());

        Assert.assertEquals((100 * 6) - legionJugador2.obtenerPuntosDeAtaqueTotal(), juegoPrueba.getJugador1().obtenerLegion().obtenerPuntosDeVidaTotal(),0.01);
        Assert.assertEquals(100*3, juegoPrueba.getJugador2().obtenerLegion().obtenerPuntosDeVidaTotal(),0.01);
    }

}

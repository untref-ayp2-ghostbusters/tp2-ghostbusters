package dados;

import java.util.Random;

public class Dado {

	private static Dado dado = new Dado();
	private Random randomizer;
	
	private Dado() {
		this.randomizer = new Random();
	}
	
	public static Dado obtenerInstancia() {
		return dado;
	}
	
	public int tirarDado() {
		return (randomizer.nextInt(6)) + 1;
	}
	
}

package juegos;

import interfaces.IMenu;
import jugadores.Jugador;
import menues.MenuInicial;

public class Juego {

    private Jugador jugador1;
    private Jugador jugador2;
    private boolean turnoJugador1;
    private boolean compraInicial;
    IMenu menu;

    public Juego() {
        this.turnoJugador1 = false;
        this.compraInicial = true;
        menu = new MenuInicial(this);
    }

    public void comenzarJuego(){
        menu.mostrarMenu();
    }

    public void setJugador1(String nombre) {
        this.jugador1 = new Jugador(nombre);
    }

    public Jugador getJugador1() {
        return this.jugador1;
    }

    public void setJugador2(String nombre) {
        this.jugador2 = new Jugador(nombre);
    }

    public Jugador getJugador2() {
        return this.jugador2;
    }

    public void setTurnoJugador1(boolean turno) {
        this.turnoJugador1 = turno;
    }

    public boolean getTurnoJugador1() {
        return this.turnoJugador1;
    }

    public void finalizarCompraInicial() {
        this.compraInicial = false;
    }

    public boolean getCompraInicial() {
        return this.compraInicial;
    }

    public boolean hayGanador() {
        return !this.jugador1.tienePuntosDeVida() || !this.jugador2.tienePuntosDeVida();
    }

    public String obtenerNombreJugadorGanador() {
        String nombre;

        if (this.jugador1.tienePuntosDeVida()) {
            nombre = this.jugador1.getNombre();
        } else {
            nombre = this.jugador2.getNombre();
        }

        return nombre;
    }

    public void atacar(Jugador jugadorAtacante, Jugador jugadorAtacado) {
        jugadorAtacante.atacar(jugadorAtacado);
        this.setTurnoJugador1(!this.getTurnoJugador1());
    }

}

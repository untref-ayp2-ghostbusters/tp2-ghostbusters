package juegos;

import unidades.Legion;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.StringTokenizer;

public abstract class CustomFileReader {

    protected String fileName;
    protected String delimiter;
    protected FileReader fileReader;
    protected BufferedReader bufferedReader;

    public CustomFileReader(String fileName, String delimiter) {
        try {
            this.fileName = fileName;
            this.delimiter = delimiter;
            this.fileReader = new FileReader(this.fileName);
            this.bufferedReader = new BufferedReader(this.fileReader);
        } catch (FileNotFoundException e) {
            System.out.println("No se encontr� el archivo '" + this.fileName + "' .");
        }
    }

    public LinkedList<Legion> readFile() {
        LinkedList<Legion> legiones = new LinkedList<>();
        String line = "";

        try {
            while ((line = this.bufferedReader.readLine()) != null) {
                StringTokenizer stringTokenizer = new StringTokenizer(line);
                String nombre = stringTokenizer.nextToken(delimiter);
                int cantidadAuxiliares = Integer.parseInt(stringTokenizer.nextToken(delimiter).trim());
                int cantidadLegionarios = Integer.parseInt(stringTokenizer.nextToken(delimiter).trim());
                int cantidadCenturiones = Integer.parseInt(stringTokenizer.nextToken(delimiter).trim());

                legiones.add(new Legion(nombre, cantidadAuxiliares, cantidadLegionarios, cantidadCenturiones));
            }
        } catch (IOException e) {
            System.out.println("Ocurrio un error al leer el archivo.");
        } catch (NumberFormatException ex) {
            System.out.println("Ocurrio un error leyendo la cantidad de unidades");
        }

        return legiones;
    }

}

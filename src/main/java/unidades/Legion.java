package unidades;

import java.util.Collection;
import java.util.TreeMap;

public class Legion extends Unidad {

    private TreeMap<Integer, Unidad> units;
    private String nombreLegion;

    public Legion(String nombreLegion, int cantidadAuxiliares, int cantidadLegionarios, int cantidadCenturiones) {
        super(cantidadAuxiliares + cantidadLegionarios + cantidadCenturiones);
        this.units = new TreeMap<>();
        this.agregarUnidad(1, new Auxiliar(cantidadAuxiliares));
        this.agregarUnidad(2, new Legionario(cantidadLegionarios));
        this.agregarUnidad(3, new Centurion(cantidadCenturiones));
        this.nombreLegion = nombreLegion;
    }

    public Collection<Unidad> obtenerUnidades() {
        return this.units.values();
    }

    public Unidad obtenerAuxiliares() {
        return this.units.get(1);
    }

    public Unidad obtenerLegionarios() {
        return this.units.get(2);
    }

    public Unidad obtenerCenturiones() {
        return this.units.get(3);
    }

    private void agregarUnidad(int ordenAtaque, Unidad unidad) {
        this.units.put(ordenAtaque, unidad);
    }

    public void agregarAuxiliares(int cantidad) {
    	this.cantidadUnidades += cantidad;
        this.units.get(1).agregarUnidades(cantidad);
    }

    public void agregarLegionarios(int cantidad) {
    	this.cantidadUnidades += cantidad;
        this.units.get(2).agregarUnidades(cantidad);
    }

    public void agregarCenturiones(int cantidad) {
        this.cantidadUnidades += cantidad;
        this.units.get(3).agregarUnidades(cantidad);
    }

    public String getNombre() {
        return this.nombreLegion;
    }

    public void setNombre(String nombreLegion) {
    	this.nombreLegion = nombreLegion;
    }
    
    @Override
    public double obtenerPuntosDeAtaqueTotal() {
        double puntosAtaqueTotal = 0;

        for (Unidad unidad : this.units.values()) {
            puntosAtaqueTotal += unidad.obtenerPuntosDeAtaqueTotal();
        }

        puntosAtaqueTotal += (puntosAtaqueTotal * 
        						(this.obtenerCenturiones().obtenerCantidadUnidades() * 0.1));

        return puntosAtaqueTotal;
    }

    public void atacar(Legion legion) {
        legion.recibirAtaque(this.obtenerPuntosDeAtaqueTotal());
    }

    @Override
    protected double recibirAtaque(double puntosAtaque) {
        double puntosAtaqueRestantes = 0;

        System.out.println("El enemigo recibio " + puntosAtaque + " puntos de ataque.");
        
        // Distribuye los puntos de ataque en todas las unidades de la legion segun su orden de ataque

        puntosAtaqueRestantes = this.units.get(1).recibirAtaque(puntosAtaque);
        puntosAtaqueRestantes = this.units.get(2).recibirAtaque(puntosAtaqueRestantes);
        puntosAtaqueRestantes = this.units.get(3).recibirAtaque(puntosAtaqueRestantes);

        System.out.println("Al enemigo le restan " + this.obtenerPuntosDeVidaTotal() + " puntos de vida.");

        return puntosAtaqueRestantes;
    }

    @Override
    public double obtenerPuntosDeVidaTotal() {
        double puntosVidaTotal = 0;

        for (Unidad unidad : this.units.values()) {
            puntosVidaTotal += unidad.obtenerPuntosDeVidaTotal();
        }

        return puntosVidaTotal;
    }

    @Override
    public int obtenerCostoTotal() {
        int costoTotal = 0;

        for (Unidad unidad : this.units.values()) {
            costoTotal += unidad.obtenerCostoTotal();
        }

        return costoTotal;
    }

    @Override
    public String toString() {
        String legion = this.nombreLegion + ": ";
        legion += this.obtenerAuxiliares().obtenerCantidadUnidades() + " Auxiliares, ";
        legion += this.obtenerLegionarios().obtenerCantidadUnidades() + " Legionarios, ";
        legion += this.obtenerCenturiones().obtenerCantidadUnidades() + " Centuriones, ";
        legion += this.obtenerPuntosDeAtaqueTotal() + " puntos de Ataque. ";
        legion += "Costo total: " + this.obtenerCostoTotal() + " creditos.";

        return legion;
    }

}

package unidades;

import armas.Arma;

public class Centurion extends Unidad {

    private Arma arma;

    public Centurion(int cantidadCenturiones) {
        super(cantidadCenturiones);
        this.costo = 200;
        this.arma = Arma.ESPADA_CORTA_Y_ESCUDO;
        this.puntosAtaque = 1;
    }

    @Override
    protected double recibirAtaque(double puntosAtaque) {
    	// Si cada centurion tiene una de dos chances de esquivar, entonces el da�o total 
    	// se divide por 2, suponiendo que la mitad de las unidades esquiva el ataque
        return super.recibirAtaque(puntosAtaque / 2);
    }

}

package unidades;

import armas.Arma;

public class Legionario extends Unidad {

    private Arma arma;

    public Legionario(int cantidadLegionarios) {
        super(cantidadLegionarios);
        this.costo = 100;
        this.arma = Arma.ESPADA_CORTA_Y_ESCUDO;
        this.puntosAtaque = 1.4;
    }

}

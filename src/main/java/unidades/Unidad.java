package unidades;

public abstract class Unidad {

    protected int cantidadUnidades;
    protected double puntosAtaque;
    protected double puntosVida;
    protected double puntosVidaTotal;
    protected int costo;

    protected Unidad(int cantidadUnidades) {
        this.cantidadUnidades = cantidadUnidades;
        this.puntosVida = 100;
        this.puntosVidaTotal = this.puntosVida * cantidadUnidades;
    }

    protected void agregarUnidades(int cantidad) {
        this.cantidadUnidades += cantidad;
        this.puntosVidaTotal += (this.puntosVida * cantidad);
    }

    protected double obtenerPuntosDeAtaqueUnitario() {
        return this.puntosAtaque;
    }

    protected double obtenerPuntosDeAtaqueTotal() {
        return this.puntosAtaque * this.cantidadUnidades;
    }

    public double obtenerPuntosDeVidaUnitario() {
        return this.puntosVida;
    }

    protected double obtenerPuntosDeVidaTotal() {
        return this.puntosVidaTotal;
    }

    public int obtenerCostoUnitario() {
        return this.costo;
    }

    public int obtenerCostoTotal() {
        return this.costo * this.cantidadUnidades;
    }

    /**
     * Recibe un ataque el da�o restante lo devuelve para poder dsitribuirse al resto de las unidades
     * @param puntosAtaque
     * @return
     */
    protected double recibirAtaque(double puntosAtaque) {
        double puntosAtaqueRestantes = puntosAtaque - this.puntosVidaTotal;

        this.puntosVidaTotal -= puntosAtaque;

        if (this.puntosVidaTotal < 0) {
            this.puntosVidaTotal = 0;
        }

        if (puntosAtaqueRestantes < 0) {
            puntosAtaqueRestantes = 0;
        }

        return puntosAtaqueRestantes;
    }

    public int obtenerCantidadUnidades() {
        return this.cantidadUnidades;
    }

}

package unidades;

import armas.Arma;

public class Auxiliar extends Unidad {

    private Arma arma;

    public Auxiliar(int cantidadAuxiliares) {
        super(cantidadAuxiliares);
        this.costo = 50;
        this.arma = Arma.JABALINA;
        this.puntosAtaque = 0.7;
    }

    @Override
    public double obtenerPuntosDeAtaqueTotal() {
    	// Si cada auxiliar tiene una de dos chances de atacar entonces el da�o total 
    	// se divide por 2, suponiendo que la mitad de las unidades falla su ataque
        return super.obtenerPuntosDeAtaqueTotal() / 2;
    }

}

package menues;

import interfaces.IMenu;
import juegos.CustomFileReader;
import juegos.FCReader;
import juegos.FPCReader;
import juegos.Juego;
import jugadores.Jugador;
import unidades.Legion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.TreeMap;

public class MenuCrearLegiones implements IMenu {

	private Juego juego;
	private BufferedReader bufferedReader;

	public MenuCrearLegiones(Juego juego) {
		this.juego = juego;
		this.bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	}

	public void mostrarMenu() {
		if (juego.getTurnoJugador1() && juego.getCompraInicial()) {
			crearNombreLegion(juego.getJugador2());
			mostrarMenuCompra(juego.getJugador2());
			crearNombreLegion(juego.getJugador1());
			mostrarMenuCompra(juego.getJugador1());
			juego.finalizarCompraInicial();
		} else if (!juego.getTurnoJugador1() && juego.getCompraInicial()) {
			crearNombreLegion(juego.getJugador1());
			mostrarMenuCompra(juego.getJugador1());
			crearNombreLegion(juego.getJugador2());
			mostrarMenuCompra(juego.getJugador2());
			juego.finalizarCompraInicial();
		} else if (juego.getTurnoJugador1()) {
			mostrarMenuCompra(juego.getJugador1());
		} else {
			mostrarMenuCompra(juego.getJugador2());
		}
	}

	private void crearNombreLegion(Jugador jugador) {
		String nombre = "";
		while (nombre == "") {
			try {
				System.out.println("Ingrese el nombre de su ejercito: ");
				nombre = bufferedReader.readLine();
			} catch (IOException ex) {
				System.out.println("El nombre no debe ser un espacio en blanco");
			}
		}
		jugador.asignarNombreLegion(nombre);
	}

	public void mostrarMenuCompra(Jugador jugador) {
		int opcion = 0;
		while (opcion == 0) {
			System.out.println("Compra su ejercito el jugador " + jugador.getNombre());
			System.out.println("Creditos disponibles: " + jugador.getCreditos());
			System.out.println("1- Comprar Legion Prearmada");
			System.out.println("2- Comprar Auxiliares");
			System.out.println("3- Comprar Legionarios");
			System.out.println("4- Comprar Centuriones");
			System.out.println("5- Terminar la compra");

			try {
				opcion = Integer.parseInt(bufferedReader.readLine());

				switch (opcion) {
				case 1:
					mostrarLegiones(jugador);
					break;
				case 2:
					comprarAuxiliares(jugador);
					break;
				case 3:
					comprarLegionarios(jugador);
					break;
				case 4:
					comprarCenturiones(jugador);
					break;
				case 5:
					return;
				default:
					System.out.println("Opcion no valida.");
					break;
				}
				opcion = 0;

			} catch (Exception e) {
				System.out.println("Opcion no valida.");
				opcion = 0;
			}
		}

	}

	public void mostrarLegiones(Jugador jugador) {
		TreeMap<Integer, Legion> legiones = new TreeMap<>();

		int contador = 1;
		for (Legion legion : obtenerLegionesDeArchivos()) {
			legiones.put(contador, legion);
			System.out.println(contador + "- " + legion.toString());
			contador++;
		}

		int opcion = 0;

		while (opcion == 0) {
			try {
				opcion = Integer.parseInt(bufferedReader.readLine());

				if (legiones.containsKey(opcion)) {
					if (jugador.consumirCreditos(legiones.get(opcion).obtenerCostoTotal())) {
						jugador.asignarLegion(legiones.get(opcion));
					}
				} else {
					System.out.println("Opcion no valida");
					opcion = 0;
				}
			} catch (Exception e) {
				System.out.println("Opcion no valida");
				opcion = 0;
			}
		}
	}

	private int compraUnidad(String unidad) {
		int cantidad = -1;
		while (cantidad == -1) {
			try {
				System.out.println("Ingrese la cantidad de " + unidad + " a comprar:");
				cantidad = Integer.parseInt(bufferedReader.readLine());
				if (cantidad < 0) {
					System.out.println("Ingrese un numero positivo valido.");
					cantidad = -1;
				}
			} catch (Exception e) {
				System.out.println("Ingrese un numero positivo valido.");
				cantidad = -1;
			}
		}
		return cantidad;
	}

	private void comprarAuxiliares(Jugador jugador) {
		int cantidad = compraUnidad("Auxiliares");
		int costoAGastar = cantidad * jugador.obtenerLegion().obtenerAuxiliares().obtenerCostoUnitario();
		if (jugador.consumirCreditos(costoAGastar)) {
			jugador.obtenerLegion().agregarAuxiliares(cantidad);
		}
	}

	private void comprarLegionarios(Jugador jugador) {
		int cantidad = compraUnidad("Legionarios");
		int costoAGastar = cantidad * jugador.obtenerLegion().obtenerLegionarios().obtenerCostoUnitario();
		if (jugador.consumirCreditos(costoAGastar)) {
			jugador.obtenerLegion().agregarLegionarios(cantidad);
		}
	}

	private void comprarCenturiones(Jugador jugador) {
		int cantidad = compraUnidad("Centuriones");
		int costoAGastar = cantidad * jugador.obtenerLegion().obtenerCenturiones().obtenerCostoUnitario();
		if (jugador.consumirCreditos(costoAGastar)) {
			jugador.obtenerLegion().agregarCenturiones(cantidad);
		}
	}

	private LinkedList<Legion> obtenerLegionesDeArchivos() {
		CustomFileReader customFileReader = new FCReader();
		LinkedList<Legion> legiones = new LinkedList<>();
		legiones.addAll(customFileReader.readFile());
		customFileReader = new FPCReader();
		legiones.addAll(customFileReader.readFile());
		return legiones;
	}

}

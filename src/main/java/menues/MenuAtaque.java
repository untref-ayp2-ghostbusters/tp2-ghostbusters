package menues;

import interfaces.IMenu;
import juegos.Juego;
import jugadores.Jugador;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MenuAtaque implements IMenu {

    private Juego juego;
    private BufferedReader bufferedReader;

    public MenuAtaque(Juego juego) {
        this.juego = juego;
        this.bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    }

    public void mostrarMenu() {
        while (!juego.hayGanador()) {
            if (this.juego.getTurnoJugador1()) {
                System.out.println("Turno del jugador 1 (" + juego.getJugador1().getNombre() + "):");
                mostrarOpciones(juego.getJugador1(), juego.getJugador2());
            } else {
                System.out.println("Turno del jugador 2 (" + juego.getJugador2().getNombre() + "):");
                mostrarOpciones(juego.getJugador2(), juego.getJugador1());
            }
        }
        System.out.println("El ganador es: " + juego.obtenerNombreJugadorGanador());
        System.out.println("Gracias por jugar a las Batallas Romanas!");
    }

    private void mostrarOpciones(Jugador jugadorAtacante, Jugador jugadorAtacado) {
        System.out.println("1- Atacar al oponente.");
        if (jugadorAtacante.getCreditos() > 0)
        	System.out.println("2- Comprar soldados.");

        int opcion = 0;
        while (opcion == 0) {
            try {
                opcion = Integer.parseInt(bufferedReader.readLine());

                switch (opcion) {
                    case 1:
                        this.juego.atacar(jugadorAtacante, jugadorAtacado);
                        break;
                    case 2:
                    	if (jugadorAtacante.getCreditos() > 0) {
                    		new MenuCrearLegiones(juego).mostrarMenu();
                    	} else {
                    		System.out.println("Opcion no valida.");
                    	}
                    	opcion = 0;
                        break;
                    default:
                        opcion = 0;
                        break;
                }

            } catch (Exception e) {
                System.out.println("Opcion no valida.");
                opcion = 0;
            }
        }
    }

}

package menues;

import dados.Dado;
import interfaces.IMenu;
import juegos.Juego;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MenuInicial implements IMenu {

    private Juego juego;
    private BufferedReader bufferedReader;

    public MenuInicial(Juego juego) {
        this.juego = juego;
        this.bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    }

    public void mostrarMenu() {
        System.out.println("Bienvenido al juego de Batallas Romanas!");
        crearJugadores();
        lanzarDados();
        MenuCrearLegiones menu = new MenuCrearLegiones(this.juego);
        menu.mostrarMenu();
        MenuAtaque menuAtaque = new MenuAtaque(this.juego);
        menuAtaque.mostrarMenu();
    }

    private void crearJugadores() {
        String nombre = null;
        while (nombre == null) {
            try {
                System.out.println("Ingrese el nombre del Jugador 1: ");
                nombre = bufferedReader.readLine();
                this.juego.setJugador1(nombre);

                System.out.println("Ingrese el nombre del Jugador 2: ");
                nombre = bufferedReader.readLine();
                this.juego.setJugador2(nombre);
            } catch (IOException e) {
                System.out.println("Opcion no valida.");
                nombre = null;
            }
        }
    }

    private void lanzarDados() {
        int valorDadoJugador1 = 0, valorDadoJugador2 = 0;

        valorDadoJugador1 = Dado.obtenerInstancia().tirarDado();
        System.out.println("El jugador 1 lanza el dado y sale: " + valorDadoJugador1);
        valorDadoJugador2 = Dado.obtenerInstancia().tirarDado();
        System.out.println("El jugador 2 lanza el dado y sale: " + valorDadoJugador2);

        while (valorDadoJugador1 == valorDadoJugador2) {
            System.out.println("Hay un empate, se lanzaran los dados de nuevo!");
            valorDadoJugador1 = Dado.obtenerInstancia().tirarDado();
            System.out.println("El jugador 1 lanza el dado y sale: " + valorDadoJugador1);
            valorDadoJugador2 = Dado.obtenerInstancia().tirarDado();
            System.out.println("El jugador 2 lanza el dado y sale: " + valorDadoJugador2);
        }

        this.juego.setTurnoJugador1(valorDadoJugador2 > valorDadoJugador1);
        if (!this.juego.getTurnoJugador1()) {
            System.out.println("Gana el jugador 1 (" + juego.getJugador1().getNombre() + "), inicia armando su ejercito.");
        } else {
            System.out.println("Gana el jugador 2 (" + juego.getJugador2().getNombre() + "), inicia armando su ejercito.");
        }
    }

}

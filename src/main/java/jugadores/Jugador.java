package jugadores;

import unidades.Legion;

public class Jugador {

	private String nombre;
	private int creditos;
	private Legion legion;

	public Jugador(String nombre) {
		this.nombre = nombre;
		this.creditos = 500000;
		this.legion = new Legion("", 0, 0, 0);
	}

	public String getNombre() {
		return this.nombre;
	}

	public int getCreditos() {
		return this.creditos;
	}

	public boolean consumirCreditos(int creditos) {
		if (this.creditos - creditos < 0) {
			System.out.println("Cantidad de creditos insuficientes para realizar la compra.");
			return false;
		} else {
			this.creditos -= creditos;
			return true;
		}
	}

	public void asignarNombreLegion(String nombreLegion) {
		this.legion.setNombre(nombreLegion);
	}
	
	public void asignarLegion(Legion legion) {
		this.legion.agregarAuxiliares(legion.obtenerAuxiliares().obtenerCantidadUnidades());
		this.legion.agregarLegionarios(legion.obtenerLegionarios().obtenerCantidadUnidades());
		this.legion.agregarCenturiones(legion.obtenerCenturiones().obtenerCantidadUnidades());
	}

	public Legion obtenerLegion() {
		return this.legion;
	}

	public void atacar(Jugador jugadorContrario) {
		this.legion.atacar(jugadorContrario.obtenerLegion());
	}

	public boolean tienePuntosDeVida() {
		return this.legion.obtenerPuntosDeVidaTotal() > 0;
	}

}
